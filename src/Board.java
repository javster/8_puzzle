import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdRandom;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anton.kuritsyn on 29.10.2016.
 */
public class Board {

    private int n;
    private int[] blocks;

    // number of blocks in incorrect positions + number of moves to reach board
    private int wrongPositions;

    // number of elements in wrong positions
    private int movesToGoal;

    private int zeroPosition;

    private int hashCode = 1;

    public Board(int[][] blocks) {
        if (blocks == null) {
            throw new NullPointerException();
        }

        n = blocks.length;
        this.blocks = new int[n * n];
        for (int index = 0; index < n * n; index++) {
            this.blocks[index] = blocks[index / n][index % n];
            hashCode = 31 * hashCode + this.blocks[index];
            processCell(index);
        }
    }

    private void processCell(int index) {
        if (blocks[index] == 0) {
            zeroPosition = index;
            return;
        }

        hashCode = 31 * hashCode + blocks[index];

        final int correctIndex = blocks[index] - 1;

        if (correctIndex != index) {
            wrongPositions++;
            movesToGoal += Math.abs(index / n - correctIndex / n) + Math.abs(index % n - correctIndex % n);
        }
    }

    private Board(int[] original, int n) {
        this.n = n;
        this.blocks = new int[original.length];
        for (int index = 0; index < original.length; index++) {
            blocks[index] = original[index];
            processCell(index);
        }
    }

    public int dimension() {
        return n;
    }

    public int hamming() {
        return wrongPositions;
    }

    public int manhattan() {
        return movesToGoal;
    }

    public boolean isGoal() {
        return movesToGoal == 0;
    }

    public Board twin() {
        for (int i = 0; i < n * n - 1; i++) {
            if (blocks[i] != 0) {
                for (int j = i + 1; j < n * n; j++) {
                    if (blocks[j] != 0) {
                        return copyAndSwap(i, j);
                    }
                }
            }
        }
        return null;
    }

    public boolean equals(Object other) {
        if (other == this) return true;
        if (other == null) return false;
        if (other.getClass() != this.getClass()) return false;
        Board board = (Board) other;
        if (n != board.n) return false;
        if (hashCode != board.hashCode) return false;
        for (int i = 0; i < n * n; i++) {
            if (blocks[i] != board.blocks[i]) {
                return false;
            }
        }
        return true;
    }

    public Iterable<Board> neighbors() {
        List<Board> neighbors = new ArrayList<>();
        if (zeroPosition % n > 0) {
            neighbors.add(copyAndSwap(zeroPosition, zeroPosition - 1));
        }
        if (zeroPosition % n < n - 1) {
            neighbors.add(copyAndSwap(zeroPosition, zeroPosition + 1));
        }

        if (zeroPosition / n > 0) {
            neighbors.add(copyAndSwap(zeroPosition, zeroPosition - n));
        }
        if (zeroPosition / n < n - 1) {
            neighbors.add(copyAndSwap(zeroPosition, zeroPosition + n));
        }

        return neighbors;
    }

    private Board copyAndSwap(int index1, int index2) {
        int[] newArray = new int[blocks.length];
        for (int i = 0; i < blocks.length; i++) {
            newArray[i] = blocks[i];
        }
        swap(newArray, index1, index2);
        return new Board(newArray, n);
    }

    private void swap(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(n + "\n");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                s.append(String.format("%2d ", blocks[i * n + j]));
            }
            s.append("\n");
        }
        return s.toString();
    }

    public static void main(String[] args) {
        // create initial board from file
        int[][] arr1 = {{0, 1}, {2, 3}};
        int[][] arr2 = {{0, 1}, {2, 3}};
        Board b1 = new Board(arr1);
        Board b2 = new Board(arr2);

        System.out.println("b1 hash = " + b1.hashCode);
        System.out.println("b2 hash = " + b1.hashCode);

        if (b1.equals(b2)) {
            System.out.println("equals");
        }
    }
}