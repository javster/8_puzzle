import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.StdOut;

/**
 * Created by anton.kuritsyn on 29.10.2016.
 */
public class Solver {

    private int move;

    private MinPQ<BoardHolder> queue;
    private MinPQ<BoardHolder> twinQueue;

    private Queue<Board> path;

    private boolean isSolved;

    private Board previousBoard;
    private Board previousTwinBoard;

    private class BoardHolder {

        private int move;
        private Board board;

        public BoardHolder(Board board, int move) {
            this.board = board;
            this.move = move;
        }

        public Board getBoard() {
            return board;
        }

        public int getPriority() {
            return board.manhattan() + board.hamming();
        }
    }

    public Solver(Board initial) {
        if (initial == null) {
            throw new NullPointerException();
        }

        queue = new MinPQ<>((o1, o2) -> {
            return o1.getPriority() - o2.getPriority();
        });

        twinQueue = new MinPQ<>((o1, o2) -> {
            return o1.getPriority() - o2.getPriority();
        });

        path = new Queue<>();

        queue.insert(new BoardHolder(initial, move));
        twinQueue.insert(new BoardHolder(initial.twin(), move));

        for (int i = 0; i < 100; i++) {

            BoardHolder boardHolder = queue.delMin();
            BoardHolder boardHolderTwin = twinQueue.delMin();

            path.enqueue(boardHolder.getBoard());
            move++;

            if (boardHolder.getBoard().isGoal()) {
                isSolved = true;
                return;
            }
            else if (boardHolderTwin.getBoard().isGoal()) {
                isSolved = false;
                return;
            }

            for (Board b : boardHolderTwin.getBoard().neighbors()) {
                if (isItNew(b, previousTwinBoard)) {
                    twinQueue.insert(new BoardHolder(b, move));
                }
            }

            for (Board b : boardHolder.getBoard().neighbors()) {
                if (isItNew(b, previousBoard)) {
                    queue.insert(new BoardHolder(b, move));
                }
            }
            previousBoard = boardHolder.getBoard();
            previousTwinBoard = boardHolderTwin.getBoard();
        }
    }

    private boolean isItNew(Board board, Board previousBoard) {
        if (previousBoard == null) {
            return true;
        }
        if (previousBoard.equals(board)) {
            return false;
        }
        return true;
    }

    public boolean isSolvable() {
        return isSolved;
    }

    public int moves() {
        return path.size() - 1;
    }

    public Iterable<Board> solution() {
        return path;
    }

    public static void main(String[] args) {
        // create initial board from file
        In in = new In("C:\\education\\eight_puzzle\\8puzzle\\" + args[0]);
        int n = in.readInt();
        int[][] blocks = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);
        StdOut.println(initial.twin());

        // solve the puzzle
        Solver solver = new Solver(initial);

        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            for (Board board : solver.solution())
                StdOut.println(board);
        }
    }

}